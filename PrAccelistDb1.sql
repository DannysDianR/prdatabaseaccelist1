create database PrAccelistDb1

use PrAccelistDb1

create table Customer(
	CustomerId int Identity(1,1) Primary Key,
	Name varchar(255) NOT NULL,
	Address varchar(255),
	Email varchar(255) NOT NULL,
	Password varchar(255) NOT NULL,
)

create table Membership(
	MembershipId int Identity(1,1) Primary Key,
	CustomerId int foreign key references Customer(CustomerId),
)

create table Admin(
	AdminId int Identity(1,1) Primary Key,
	Name varchar(255) NOT NULL,
	Address varchar(255),
	Email varchar(255) NOT NULL,
	Password varchar(255) NOT NULL,
)

create table Forum(
	ForumId int Identity(1,1) Primary Key,
	CustomerId int foreign key references Customer(CustomerId),
	MembershipId int foreign key references Membership(MembershipId),
	Name varchar(255),
	
)

create table Report(
	ReportId int Identity(1,1) Primary Key,
	ForumId int foreign key references Forum(ForumId),
	AdminId int foreign key references Admin(AdminId),
	Name varchar(255),
)

create table ReportCategory(
	ReportCategoryId int Identity(1,1) Primary Key,
	ReportId int foreign key references Report(ReportId),
	Category varchar(255),
)

create table Video(
	VideoId int Identity(1,1) Primary Key,
	AdminId int foreign key references Admin(AdminId),
	Name varchar(255) NOT NULL,
	CreatedAt datetimeoffset Default Getdate(),
	CreatedBy varchar(255) Default 'Admin',
	UpdateAt datetimeoffset Default Getdate(),
	UpdateBy varchar(255) Default 'Admin',
)

create table VideoPremium(
	VideoPremiumId int Identity(1,1) Primary Key,
	VideoId int foreign key references Video(VideoId),
	Name varchar(255) NOT NULL,

)

create table VideoCategory(
	VideoCategoryId int Identity(1,1) Primary Key,
	VideoId int foreign key references Video(VideoId),
	VideoPremiumId int foreign key references VideoPremium(VideoPremiumId),
	Category varchar(255) NOT NULL
)

create table Ads(
	AdsId int Identity(1,1) Primary Key,
	VideoId int foreign key references Video(VideoId),
	Name varchar(255) NOT NULL,
)

create table Streaming(
	StreamingId int Identity(1,1) Primary Key,
	CustomerId int foreign key references Customer(CustomerId),
	MembershipId int foreign key references Membership(MembershipId),
	VideoId int foreign key references Video(VideoId),
)

create table News(
	NewsId int Identity(1,1) Primary Key,
	CustomerId int foreign key references Customer(CustomerId),
	AdminId int foreign key references Admin(AdminId),
	MembershipId int foreign key references Membership(MembershipId),
	Name varchar(255),
	CreatedAt datetimeoffset Default Getdate(),
	CreatedBy varchar(255) Default 'Admin',
	UpdateAt datetimeoffset Default Getdate(),
	UpdateBy varchar(255) Default 'Admin',
)

create table Promo(
	PromoId int Identity(1,1) Primary Key,
	NewsId int foreign key references News(NewsId),
	AdminId int foreign key references Admin(AdminId),
	Name varchar(255),
)

create table NewsCategory(
	NewsCategoryId int Identity(1,1) Primary Key,
	NewsId int foreign key references News(NewsId),
	Category varchar(255)
)


drop table Promo,NewsCategory,Ads,VideoCategory,Streaming,ReportCategory,Report,Forum,VideoPremium,Membership,News,Video,Admin,Customer











